class ParameterTypeError(Exception):
	""" Exception raised when parameter types are not as expected 
		methodName: The name of the function/method with the mismatch
		type1: Expected type
		type2: Given type
	"""
	def __init__(self,moduleAndClassName,methodName,type1,type2):
		self.value="Mismatched parameter types in {0}.{1}: Expected {2}, given {3}".format(moduleAndClassName,methodName,type1, type2)
	def __str__(self):
		return repr(self.value)

