import Exceptions

def accepts(*argTypes):
	"""
		Takes types as arguments and compares them
		to the types of the arguments of the method
		that @accepts decorates

		Not very general at the moment.
		Doesn't work with derived classes unless you
			specify the exact name of the concrete class
	"""
	def check_args(meth):
		def new(self, *args, **kwargs):
			if len(argTypes) != len(args): raise Exception(argTypes,args)
			for expectedType, givenType in zip(argTypes,[type(a) for a in args]):
				if expectedType != givenType:
					raise Exceptions.ParameterTypeError(self.__class__, \
						meth.__name__, expectedType, givenType)
			return meth(self, *args, **kwargs)
		return new
	return check_args


