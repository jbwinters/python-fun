
def rev_string(s):
    """reverse a string without using reversed() or [::-1]"""
    li = list(s)
    for i, j in zip(xrange(len(s)/2), xrange(len(s)-1, len(s)/2, -1)):
        li[i], li[j] = li[j], li[i]
    return ''.join(li)

rev_string('1234')
rev_string('12345')

