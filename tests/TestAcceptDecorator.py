import unittest
from utilities import Exceptions, Decorators

class TestAcceptDecorator(unittest.TestCase):
	class TestMethods(object):
		@Decorators.accepts(int)
		def methOneArg(self,a):
			return True

		@Decorators.accepts(int,str,float)
		def methThreeArgs(self,a,b,c):
			return True

		@Decorators.accepts(int,str,float)
		def methAcceptsThreeArgsUsesOne(self,a):
			return False


	def setUp(self):
		self.testMethods=self.TestMethods()

	def testOneIntTypeMatchingShouldNotRaiseException(self):
		# accept(int) matches with method(int)
		self.assertTrue(self.testMethods.methOneArg(12))
	
	def testOneIntTypeNotMatchingShouldRaiseException(self):
		# exception raised when accept(int) does not match method(str)
		self.assertRaises(Exceptions.ParameterTypeError, \
				self.testMethods.methOneArg,"some str")

	def testThreeTypesMatchingShouldNotRaiseException(self):
		#accept(int,str,float) should match method(int,str,float)
		self.assertTrue(self.testMethods.methThreeArgs(12,"12",12.))

	def testThreeTypesNotMatchingShouldRaiseException(self):
		#accept(int,str,float) should match method(int,str,float)
		self.assertRaises(Exceptions.ParameterTypeError, \
				self.testMethods.methThreeArgs,12,"12",12)

	def testMismatchedNumberOfArgumentsShouldRaiseException(self):
		#accept() called with three arguments on a method with one argument
		#should raise an exception
		self.assertRaises(Exception, self.testMethods.methAcceptsThreeArgsUsesOne,12)
	

	# etc...

