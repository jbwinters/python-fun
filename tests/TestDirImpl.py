import unittest
#from utilities.dir_implementation import dir_impl
from utilities.dir_implementation import dir_impl

class T(object):
    def t(self):
        pass

class TS(T):
    def s(self):
        pass

class O():
    def o(self):
        pass

class OS(O):
    def s(self):
        pass


class TestDirImpl(unittest.TestCase):
    def test_dir_impl_equals_dir_for_type(self):
        self.assertTrue(sorted(dir_impl(type)) == sorted(dir(type)))

    def test_class_with_object_base(self):
        self.assertTrue(sorted(dir_impl(T)) == sorted(dir(T)))
    def test_instance_with_object_parent_class(self):
        t = T()
        self.assertTrue(sorted(dir_impl(t)) == sorted(dir(t)))

    def test_child_class_of_class_with_object_parent(self):
        self.assertTrue(sorted(dir_impl(TS)) == sorted(dir(TS)))
    def test_instance_with_child_class_derived_from_object_base(self):
        ts = TS()
        self.assertTrue(sorted(dir_impl(ts)) == sorted(dir(ts)))

    def test_classobj_class(self):
        self.assertTrue(sorted(dir_impl(O)) == sorted(dir(O)))
    def test_classobj_instance(self):
        o = O()
        self.assertTrue(sorted(dir_impl(o)) == sorted(dir(o)))

    def test_classobj_derived_class(self):
        self.assertTrue(sorted(dir_impl(OS)) == sorted(dir(OS)))
    def test_instance_with_classobj_derived_class(self):
        os = OS()
        self.assertTrue(sorted(dir_impl(os)) == sorted(dir(os)))

    def test_on_modules(self):
        import __builtin__
        self.assertTrue(sorted(dir_impl(__builtin__)) == sorted(dir(__builtin__)))
        import sys
        self.assertTrue(sorted(dir_impl(sys)) == sorted(dir(sys)))

    def test_on_NoneType(self):
        self.assertTrue(sorted(dir_impl(None)) == sorted(dir(None)))

    def test_on_package_module(self):
        # will probably be None in general
        self.assertTrue(sorted(dir_impl(__package__)) == sorted(dir(__package__)))

    def test_no_parameters(self):
        o=T()
        self.assertTrue(sorted(dir_impl()) == sorted(dir()))

    def test_too_many_parameters(self):
        with self.assertRaises(TypeError):
            dir_impl(1, 2)

    def test_slice_type(self):
        import types
        li = [1,3]
        self.assertEqual(dir_impl(types.SliceType(li)),
                dir(types.SliceType(li)))


    def test_dir(self):
        """ Python's dir() tests """

        import sys
        import types

        # dir_impl(wrong number of arguments)
        self.assertRaises(TypeError, dir_impl, 42, 42)

        # dir_impl() - local scope
        local_var = 1
        self.assertIn('local_var', dir_impl())

        # dir_impl(module)
        self.assertIn('exit', dir_impl(sys))

        # dir_impl(module_with_invalid__dict__)
        class Foo(types.ModuleType):
            __dict__ = 8
        f = Foo("foo")
        self.assertRaises(TypeError, dir_impl, f)

        # dir_impl(type)
        self.assertIn("strip", dir_impl(str))
        self.assertNotIn("__mro__", dir_impl(str))

        # dir_impl(obj)
        class Foo(object):
            def __init__(self):
                self.x = 7
                self.y = 8
                self.z = 9
        f = Foo()
        self.assertIn("y", dir_impl(f))

        # dir_impl(obj_no__dict__)
        class Foo(object):
            __slots__ = []
        f = Foo()
        self.assertIn("__repr__", dir_impl(f))

        # dir_impl(obj_no__class__with__dict__)
        # (an ugly trick to cause getattr(f, "__class__") to fail)
        class Foo(object):
            __slots__ = ["__class__", "__dict__"]
            def __init__(self):
                self.bar = "wow"
        f = Foo()
        self.assertNotIn("__repr__", dir_impl(f))
        self.assertIn("bar", dir_impl(f))

        # dir_impl(obj_using __dir__)
        class Foo(object):
            def __dir__(self):
                return ["kan", "ga", "roo"]
        f = Foo()
        self.assertTrue(dir_impl(f) == ["ga", "kan", "roo"])

        # dir_impl(obj__dir_impl__not_list)
        class Foo(object):
            def __dir__(self):
                return 7
        f = Foo()
        self.assertRaises(TypeError, dir_impl, f)

        # dir_impl(traceback)

        # This one was originally different but seems to be wrong
        try:
            raise IndexError
        except:
            self.assertEqual(dir_impl(sys.exc_info()[2]),
                            dir(sys.exc_info()[2]))
            #self.assertEqual(len(dir(sys.exc_info()[2])), 4)
