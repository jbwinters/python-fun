import array

class Queue(object):
    """
    Fixed-length queue
    Based on the implementation in Udacity's Software Testing course
    Basically a circular list
    """
    def __init__(self, max_length):
        print 'init'
        assert max_length > 0
        self.head = 0
        self.tail = 0
        self.max_length = max_length
        self.length = 0
        self.data = array.array('i', [0]*max_length)
    def is_empty(self):
        return self.length == 0
    def is_full(self):
        return self.length == self.max_length
    def enqueue(self, value):
        if self.is_full():
            return False
        self.data[self.tail] = value
        self.tail += 1
        self.length += 1
        if self.tail == self.max_length:
            self.tail = 0
    def dequeue(self):
        if self.is_empty():
            return None
        popped = self.data[self.head]
        self.head += 1
        self.length -= 1
        if self.head == self.max_length:
            self.head = 0
        return popped

