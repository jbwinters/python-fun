"""
I was asked how to implement dir and had no idea where to start.
Without looking up a solution, this is what I came up with.
"""

import inspect
import types
def dir_impl(*args):
    if len(args) > 1:
        # too many parameters
        raise TypeError("dir_impl expected at most 1 arguments, got %d"
                            % len(args))
    elif not len(args):
        # no parameters given,
        # return the list of names in the caller's local scope
        return inspect.currentframe().f_back.f_locals.keys()
    obj =  args[0]
    if hasattr(obj, '__dir__'):
        return sorted(obj.__dir__())
    if isinstance(obj, types.NoneType):
        # obj is None
        obj = object

    def get_attrs(obj):
        if not hasattr(obj, '__dict__'): return [] # slots only
        if not isinstance(obj.__dict__, types.DictType)\
                and not isinstance(obj.__dict__, types.DictProxyType):
            raise TypeError("%s.__dict__ is not a dictionary" % obj.__name__)
        return obj.__dict__.keys()

    if isinstance(obj, types.ModuleType):
        # obj is a module
        return get_attrs(obj)
    attrs = set()
    if not hasattr(obj, '__bases__'):
        # obj is an instance
        if not hasattr(obj, '__class__'):
            # slots
            return get_attrs(obj)
        klass = obj.__class__
        attrs.update(get_attrs(klass))
    else:
        # obj is a class
        klass =  obj
    # add all of klass' ancestors'
    # (i.e. obj's ancestors') attributes to the set
    for cls in klass.__bases__:
        attrs.update(get_attrs(cls))
        attrs.update(dir_impl(cls))
    attrs.update(get_attrs(obj))
    return list(sorted(attrs))
